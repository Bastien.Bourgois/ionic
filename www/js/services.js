angular.module('starter.services', [])

.factory('VeillesFactory', function($http){
    return {
        getAll:function(){
            return $http.get('http://veille.popschool.fr/api/?api=veille&action=getAll');
        },
        get:function(veilleId) {
            return $http.get('http://veille.popschool.fr/api/?api=veille&action=get&id='+veilleId);
        }
    }
})

.factory('UsersFactory', function($http){
    return {
        getAll:function(){
            return $http.get('http://veille.popschool.fr/api/?api=user&action=getAll');
        },
        get:function(userId){
            return $http.get('http://veille.popschool.fr/api/?api=user&action=get&id='+userId);
        },
        getbyId:function(userId){
            return $http.get('http://veille.popschool.fr/api/?api=veille&action=getByUser&id_user=' + userId);
        }
    }
})

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Luke Cage',
    lastText: 'lol tro maran',
    face: 'img/luke.jpg'
  }, {
    id: 1,
    name: 'Matthew Murdock',
    lastText: 'J\'avoue que c\'est drôle',
    face: 'img/daredevil.jpg'
  }, {
    id: 2,
    name: 'Jessica Jones',
    lastText: 'Ouais',
    face: 'img/jess.jpg'
  }, {
    id: 3,
    name: 'Danny Rand',
    lastText: 'Look at my coconuts!',
    face: 'img/danny.jpg'
  }, {
    id: 4,
    name: 'Claire Temple',
    lastText: 'This is wicked good ice cream.',
    face: 'img/claire.jpg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
